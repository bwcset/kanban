﻿/*
 * Project      : ASQL Final Project
 * Developer(s) : Nathan Bray, Gabriel Paquette
 * Date Created : 2017-04-01
 * Description  : This class is to provide us with accessing our data so
 * we may update the fields after the users changes a configuration
 */

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace KanbanObjects
{
    public class DataAccess
    {
        private readonly SqlConnection _conn;

        public DataAccess()
        {
            _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kanbanConnectionString"].ConnectionString);
        }

        /// <summary>
        /// this method commits the users changes to the database
        /// </summary>
        /// <param name="value">the new value</param>
        /// <param name="cName">the name of the configuration</param>
        public void UpdateValue(double value, string cName)
        {
            using (SqlCommand cmd = new SqlCommand("dbo.UpdateConfiguration", _conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@cName", SqlDbType.VarChar);
                cmd.Parameters.Add("@cValue", SqlDbType.VarChar);

                cmd.Parameters["@cName"].Value = cName;
                cmd.Parameters["@cValue"].Value = value.ToString();
                // open connection and execute stored procedure
                if (_conn.State == ConnectionState.Closed) _conn.Open();
                cmd.ExecuteNonQuery();
                _conn.Close();
            }
        }

        /// <summary>
        /// This method gets the kanban cards in the common try
        /// </summary>
        /// <param name="lastRun">a time stamp of the last time the runner came by</param>
        /// <returns>returns a list of the kanban cards</returns>
        public List<Bin> GetCommonTray(DateTime lastRun)
        {
            List<Bin> bins = new List<Bin>();

            using (SqlCommand cmd = new SqlCommand("dbo.DumpCommonTray", _conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@LastRunTime", SqlDbType.DateTime);
                cmd.Parameters["@LastRunTime"].Value = lastRun;
                // open connection and execute stored procedure
                if (_conn.State == ConnectionState.Closed) _conn.Open();

                using (SqlDataReader dataReader = cmd.ExecuteReader())
                {
                    //get the bin values
                    while (dataReader.Read())
                    {
                        Bin b = new Bin();

                        b.BinId = Convert.ToInt32(dataReader["BinID"]);

                        bins.Add(b);
                    }
                }

                _conn.Close();
            }

            return bins;
        }

        /// <summary>
        /// this function gets the timescale factor from the config table
        /// </summary>
        /// <returns>returns the timescale </returns>
        public int GetTimeScale()
        {
            int ts = 0;
            using (SqlCommand cmd = new SqlCommand("dbo.GetTimescale", _conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                // open connection and execute stored procedure
                if (_conn.State == ConnectionState.Closed) _conn.Open();

                using (SqlDataReader dataReader = cmd.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        // read output value from @NewId
                        ts = Convert.ToInt32(dataReader["cValue"]);
                    }
                }
                _conn.Close();
            }
            return ts;
        }

        /// <summary>
        /// this method will get the threshold of at what quantity a kanban card is created
        /// </summary>
        /// <returns>returns the threshold </returns>
        public int GetRefillThreshold()
        {
            int ts = 0;
            using (SqlCommand cmd = new SqlCommand("dbo.GetPartRefillThreshold", _conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                // open connection and execute stored procedure
                if (_conn.State == ConnectionState.Closed) _conn.Open();

                using (SqlDataReader dataReader = cmd.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        // read output value from @@threshold
                        ts = Convert.ToInt32(dataReader["@threshold"]);
                    }
                }
                _conn.Close();
            }
            return ts;
        }

        /// <summary>
        /// This function calls the stored procedure which makes a work station and all
        /// the nessesary data for a station to work.
        /// </summary>
        /// <returns>returns the newly created workstation's ID</returns>
        public int CreateWorkstation()
        {
            using (SqlCommand cmd = new SqlCommand("dbo.MakeWorkstation", _conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter retval = cmd.Parameters.Add("@Wid", SqlDbType.Int);
                retval.Direction = ParameterDirection.ReturnValue;
                // open connection and execute stored procedure
                if (_conn.State == ConnectionState.Closed) _conn.Open();
                cmd.ExecuteNonQuery();

                // read output value from @Wid
                int workstationId = Convert.ToInt32(cmd.Parameters["@Wid"].Value);
                _conn.Close();

                return workstationId;
            }
        }

        /// <summary>
        /// This function calls the stored procedure which makes a work station and all
        /// the nessesary data for a station to work.
        /// </summary>
        /// <returns>returns the newly created workstation's ID</returns>
        public void ReplenishQueue(int binId)
        {
            using (SqlCommand cmd = new SqlCommand("dbo.MakeReplenishQueue", _conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@OldBinID", SqlDbType.Int);
                cmd.Parameters["@OldBinID"].Value = binId;
                // open connection and execute stored procedure
                if (_conn.State == ConnectionState.Closed) _conn.Open();
                cmd.ExecuteNonQuery();
                _conn.Close();
            }
        }

        /// <summary>
        /// gets the information that coresponds to the workstation
        /// </summary>
        /// <param name="workId"> the work station id</param>
        /// <param name="defect"> the defect rate for the employee</param>
        /// <param name="assembly">the assembly rate of the employee</param>
        public void GetWorkstationInfo(int workId, out double defect, out double assembly)
        {
            using (SqlCommand cmd = new SqlCommand("dbo.WorkstationTypeInfo", _conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@workID", SqlDbType.Int);
                cmd.Parameters["@workID"].Value = workId;

                cmd.Parameters.Add("@AssemblyTime", SqlDbType.Float).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@DefectRate", SqlDbType.Float).Direction = ParameterDirection.Output;

                // open connection and execute stored procedure
                if (_conn.State == ConnectionState.Closed) _conn.Open();
                cmd.ExecuteNonQuery();

                // read output value from @NewId
                defect = Convert.ToDouble(cmd.Parameters["@DefectRate"].Value);
                assembly = Convert.ToDouble(cmd.Parameters["@AssemblyTime"].Value);

                _conn.Close();
            }
        }

        /// <summary>
        /// the function reduces the parts from the bins in use at a work station by 1
        /// </summary>
        /// <param name="workstationId">The work station ID</param>
        /// <returns></returns>
        public bool ReduceParts(int workstationId)
        {
            using (SqlCommand cmd = new SqlCommand("dbo.ReduceParts", _conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter retval = cmd.Parameters.Add("@retCode", SqlDbType.Bit);
                retval.Direction = ParameterDirection.ReturnValue;

                cmd.Parameters.Add("@WorkID", SqlDbType.Int);
                cmd.Parameters["@WorkID"].Value = workstationId;

                // open connection and execute stored procedure
                if (_conn.State == ConnectionState.Closed) _conn.Open();
                cmd.ExecuteNonQuery();
                bool reduced = Convert.ToBoolean(cmd.Parameters["@retCode"].Value);
                _conn.Close();

                return reduced;
            }
        }

        /// <summary>
        /// This method will create a foglamp entry in the database
        /// reporting on when it was created as well as if it is defective
        /// </summary>
        /// <param name="defective">this is whether it is defective or not</param>
        /// <param name="workId">the workstation id</param>
        public void AddFoglamp(bool defective, int workId)
        {
            using (SqlCommand cmd = new SqlCommand("dbo.CreateFogLamp", _conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@Wid", SqlDbType.Int);
                cmd.Parameters["@Wid"].Value = workId;

                cmd.Parameters.Add("@defective", SqlDbType.Bit);
                cmd.Parameters["@defective"].Value = defective;

                // open connection and execute stored procedure
                if (_conn.State == ConnectionState.Closed) _conn.Open();
                cmd.ExecuteNonQuery();
                _conn.Close();
            }
        }

        /// <summary>
        /// This method will retrieve the default assembly time the worker will use as a time to
        /// create the foglamp
        /// </summary>
        /// <returns>the default amount of time it takes and average worker to make a fog lamp</returns>
        public int GetDefaultAssemblyTime()
        {
            int time = 0;
            using (SqlCommand cmd = new SqlCommand("dbo.GetDefaultAssemblyTime", _conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                // open connection and execute stored procedure
                if (_conn.State == ConnectionState.Closed) _conn.Open();

                using (SqlDataReader dataReader = cmd.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        // read output value from @NewId
                        time = Convert.ToInt32(dataReader["cValue"]);
                    }
                }

                _conn.Close();
            }

            return time;
        }

        /// <summary>
        /// This method will retrieve the Bins that belong to a workstation and that they are currently
        /// to be using in their assembly process
        /// </summary>
        /// <param name="workId">the id of the workstation</param>
        /// <param name="withMax">indicates whether we want to incluse the maximum amount
        /// each part can have or not</param>
        /// <returns>a list of the workstations bins</returns>
        public List<Bin> GetBins(int workId, bool withMax = false)
        {
            List<Bin> bins = new List<Bin>();
            string proc = "dbo.GetBins";
            if (withMax)
            {
                proc = "dbo.GetAndonBins";
            }

            using (SqlCommand cmd = new SqlCommand(proc, _conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@workID", SqlDbType.Int);
                cmd.Parameters["@workID"].Value = workId;
                // open connection and execute stored procedure
                if (_conn.State == ConnectionState.Closed) _conn.Open();

                using (SqlDataReader dataReader = cmd.ExecuteReader())
                {
                    //get the bin values
                    while (dataReader.Read())
                    {
                        Bin b = new Bin();

                        b.BinId = Convert.ToInt32(dataReader["BinID"]);
                        b.PartId = Convert.ToInt32(dataReader["PartID"]);
                        b.Quantity = Convert.ToInt32(dataReader["PartQuantity"]);
                        if (withMax)
                        {
                            b.MaxValue = Convert.ToInt32(dataReader["cValue"]);
                        }

                        bins.Add(b);
                    }
                    _conn.Close();
                }
            }

            return bins;
        }

        /// <summary>
        /// This method gets the interval rate at which the runner will cycle past the workstation and
        /// retrieve the kanban cards from the common tray
        /// </summary>
        /// <returns>the amount of minutes</returns>
        public int GetRunnerInterval()
        {
            int runnerInterval = 0;
            using (SqlCommand cmd = new SqlCommand("dbo.GetRunnerTime", _conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                // open connection and execute stored procedure

                if (_conn.State == ConnectionState.Closed) _conn.Open();
                using (SqlDataReader dataReader = cmd.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        // read output value from @NewId
                        runnerInterval = Convert.ToInt32(dataReader["cValue"]);
                    }
                }

                _conn.Close();

                return runnerInterval;
            }
        }

        /// <summary>
        /// This method will check the state of the simulation
        /// </summary>
        /// <returns>the state (whether it is active or not)</returns>
        public int GetSimulationState()
        {
            using (SqlCommand cmd = new SqlCommand("dbo.GetState", _conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter retval = cmd.Parameters.Add("@state", SqlDbType.Bit);
                retval.Direction = ParameterDirection.ReturnValue;
                // open connection and execute stored procedure
                if (_conn.State == ConnectionState.Closed) _conn.Open();
                cmd.ExecuteNonQuery();

                // read output value from @Wid
                int simState = Convert.ToInt32(cmd.Parameters["@state"].Value);
                _conn.Close();

                return simState;
            }
        }

        /// <summary>
        /// This method will set a bool within the database to indicate
        /// that the simulation should pause
        /// </summary>
        public void PauseSimulation()
        {
            using (SqlCommand cmd = new SqlCommand("dbo.OnPause", _conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                // open connection and execute stored procedure
                if (_conn.State == ConnectionState.Closed) _conn.Open();
                cmd.ExecuteNonQuery();
                _conn.Close();
            }
        }

        /// <summary>
        /// This method will set a bool within the database to indicate
        /// that the simulation should resume
        /// </summary>
        public void ResumeSimulation()
        {
            using (SqlCommand cmd = new SqlCommand("dbo.OnResume", _conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                // open connection and execute stored procedure
                if (_conn.State == ConnectionState.Closed) _conn.Open();
                cmd.ExecuteNonQuery();
                _conn.Close();
            }
        }

        /// <summary>
        /// This method will retrieve the default assembly time the worker will use as a time to
        /// create the foglamp
        /// </summary>
        /// <returns>the default amount of time it takes and average worker to make a fog lamp</returns>
        public List<int> GetWorkstationIDs()
        {
            List<int> workIDs = new List<int>();
            using (SqlCommand cmd = new SqlCommand("dbo.GetWorkstationIDs", _conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                // open connection and execute stored procedure

                if (_conn.State == ConnectionState.Closed) _conn.Open();
                using (SqlDataReader dataReader = cmd.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        // read output value from @NewId
                        workIDs.Add(Convert.ToInt32(dataReader["WorkshopID"]));
                    }
                }

                _conn.Close();
            }

            return workIDs;
        }

        /// <summary>
        /// This method will check the state of the simulation
        /// </summary>
        /// <returns>the state (whether it is active or not)</returns>
        public void TurnOffWorkstation(int workId)
        {
            using (SqlCommand cmd = new SqlCommand("dbo.UpdateWorkstationState", _conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@workID", SqlDbType.Int);
                cmd.Parameters["@workID"].Value = workId;
                // open connection and execute stored procedure
                if (_conn.State == ConnectionState.Closed) _conn.Open();
                cmd.ExecuteNonQuery();
                _conn.Close();
            }
        }

        /// <summary>
        /// This function calls the stored procedure which makes a work station and all
        /// the nessesary data for a station to work.
        /// </summary>
        /// <returns>returns the newly created workstation's ID</returns>
        public int GetGoodLampCount()
        {
            int num = 0;
            using (SqlCommand cmd = new SqlCommand("dbo.GetGoodLampCount", _conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter retval = cmd.Parameters.Add("@GoodFogCount", SqlDbType.Int);
                retval.Direction = ParameterDirection.ReturnValue;

                // open connection and execute stored procedure
                if (_conn.State == ConnectionState.Closed) _conn.Open();

                cmd.ExecuteNonQuery();
                // read output value from @NewId
                num = Convert.ToInt32(cmd.Parameters["@GoodFogCount"].Value);

                _conn.Close();
            }

            return num;
        }

        /// <summary>
        /// This function calls the stored procedure which makes a work station and all
        /// the nessesary data for a station to work.
        /// </summary>
        /// <returns>returns the newly created workstation's ID</returns>
        public int GetTotalLampCount()
        {
            int num = 0;
            using (SqlCommand cmd = new SqlCommand("dbo.GetLampCount", _conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter retval = cmd.Parameters.Add("@FogCount", SqlDbType.Int);
                retval.Direction = ParameterDirection.ReturnValue;

                // open connection and execute stored procedure
                if (_conn.State == ConnectionState.Closed) _conn.Open();

                cmd.ExecuteNonQuery();
                // read output value from @NewId
                num = Convert.ToInt32(cmd.Parameters["@FogCount"].Value);

                _conn.Close();
            }

            return num;
        }

        /// <summary>
        /// This function calls the stored procedure which makes a work station and all
        /// the nessesary data for a station to work.
        /// </summary>
        /// <returns>returns the newly created workstation's ID</returns>
        public int GetDefectLampCount()
        {
            int num = 0;
            using (SqlCommand cmd = new SqlCommand("dbo.GetDefectCount", _conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter retval = cmd.Parameters.Add("@FogCount", SqlDbType.Int);
                retval.Direction = ParameterDirection.ReturnValue;

                // open connection and execute stored procedure
                if (_conn.State == ConnectionState.Closed) _conn.Open();

                cmd.ExecuteNonQuery();
                // read output value from @NewId
                num = Convert.ToInt32(cmd.Parameters["@FogCount"].Value);

                _conn.Close();
            }

            return num;
        }
    }
}