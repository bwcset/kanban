﻿/*
 * Project      : ASQL Final Project
 * Developer(s) : Nathan Bray, Gabriel Paquette
 * Date Created : 2017-04-20
 * Description  : This class handles keeping track of a FogLamps defective rate
 *                and will assign a defect rate to the foglamp
 */

using System;

namespace KanbanObjects
{
    public class FogLamp
    {
        // defective
        public bool IsDefective { get; set; }

        /// <summary>
        /// This method will take a defect rate, and determine whether the specified foglamp
        /// is a defect or not.
        /// </summary>
        /// <param name="defectRate">The rate that is set for the foglamp to defect.
        /// This comes from the type of employee that is working at the workstation.</param>
        public FogLamp(double defectRate)
        {
            IsDefective = false;

            defectRate *= 100;
            Random rand = new Random(DateTime.Now.Millisecond);
            if (rand.Next() % 10000 < defectRate)
            {
                IsDefective = true;
                Console.WriteLine("Defective Lamp was created");
            }
        }
    }
}