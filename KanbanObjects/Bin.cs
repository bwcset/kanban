﻿/*
* Project      : ASQL Final Project
* Developer(s) : Nathan Bray, Gabriel Paquette
* Date Created : 2017-04-20
* Description  : This class handles Bin objects which
*                holds information about bins and parts
*                for the bins
*/

namespace KanbanObjects
{
    public class Bin
    {
        public int BinId { get; set; }
        public int PartId { get; set; }
        public int Quantity { get; set; }
        public int MaxValue { get; set; }

        /// <summary>
        /// This is a contructor for Bin which takes in a
        /// BinID, PartID, Quantity, and Max Quantity/
        /// </summary>
        /// <param name="b">BinID</param>
        /// <param name="p">PartID</param>
        /// <param name="q">Quantity</param>
        /// <param name="max">Max Quantityt/param>
        public Bin(int b, int p, int q, int max = 0)
        {
            BinId = b;
            PartId = p;
            Quantity = q;
            MaxValue = max;
        }

        /// <summary>
        /// This is a default constructor for the Bin Object.
        /// It sets all properties to 0
        /// </summary>
        public Bin()
        {
            BinId = 0;
            PartId = 0;
            Quantity = 0;
            MaxValue = 0;
        }
    }
}