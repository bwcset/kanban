/********************************************
 * Project: ASQL Project Milestone 1
 * Authors: Gabriel Paquette, Nathaniel Bray
 * Description: This script file creates a 
 * temporary table to store our proposed
 * configuraton table, and some defaults
 * UPDATE: This script know handles the 
 * creation of all elements our Kanban system 
 * needs.
 *******************************************/
USE master;

/*******************************
* DATABASE CREATION            *
********************************/
IF EXISTS(select * from sys.databases where name='Kanban')
BEGIN
DROP DATABASE Kanban;
END
CREATE DATABASE Kanban;
GO

/*******************************
* USER CREATION                *
********************************/
USE [master]
GO
IF NOT EXISTS (SELECT loginname FROM master.dbo.syslogins WHERE name = 'UserKanban' AND dbname = 'Kanban')
BEGIN
/* For security reasons the login is created disabled and with a random password. */
/****** Object:  Login [UserKanban]    Script Date: 4/26/2017 4:28:52 AM ******/
CREATE LOGIN  [UserKanban] WITH PASSWORD=N'n0rBertPa$sw0Rds', DEFAULT_DATABASE=[master], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=ON, CHECK_POLICY=ON

EXEC sp_addsrvrolemember 
    @loginame = N'UserKanban', 
    @rolename = N'sysadmin';

END;
GO
/*******************************
* TABLE CREATION               *
********************************/
IF EXISTS(select * from sys.databases where name='Kanban')
USE Kanban;
GO

-- Create a configuration table 
CREATE TABLE Config (
	cName varchar(50) NOT NULL,
	cValue varchar(25) NOT NULL,
	cDescription varchar(255)
	PRIMARY KEY(cName)
);

-- Create a table to handle the state of the simulation
CREATE TABLE SimulationState (
	isActive bit
);

-- Create Parts Table 
CREATE TABLE Parts(
	PartID INT NOT NULL,
	PartName nvarchar(50) NOT NULL,
	Primary KEY(PartID)
);

-- Create EmployeeType 
CREATE TABLE EmployeeType(
	TypeID int NOT NULL,
	LevelOfExperience varchar(25)
	PRIMARY KEY(TypeID)
);

-- Create Workshop 
CREATE TABLE Workshop (
	WorkshopID int NOT NULL IDENTITY(1,1),
	EmployeeTypeID int NOT NULL,
	IsActive bit NOT NULL
	Primary Key(WorkshopID)	
	FOREIGN KEY(EmployeeTypeID) REFERENCES EmployeeType(TypeID)
); 

-- Create Bin 
CREATE TABLE Bin (
	BinID INT NOT NULL IDENTITY(1,1),
	PartID INT NOT NULL,
	WorkshopID INT NOT NULL,
	PartQuantity INT CHECK(PartQuantity >= 0),
	InUse BIT NOT NULL
	PRIMARY KEY(BinID)
	FOREIGN KEY(PartID) REFERENCES Parts(PartID),
	FOREIGN KEY(WorkshopID) REFERENCES Workshop(WorkshopID)
);

-- Create CommonTray 
CREATE TABLE CommonTray(
	KanbanCardID INT NOT NULL IDENTITY(1,1),
	BinID INT NOT NULL UNIQUE,
	EntryTime DATETIME NOT NULL DEFAULT GETDATE(),
	Primary KEY(KanbanCardID),
	FOREIGN KEY (BinID) References Bin(BinID)
);

-- Create TestTray 
CREATE TABLE TestTray(
	TestTrayID INT NOT NULL IDENTITY(1,1),
	WorkshopID INT NOT NULL,
	LampCount INT NOT NULL,
	Primary KEY(TestTrayID),
	FOREIGN KEY (WorkshopID) References Workshop(WorkshopID)
);

-- Create FogLamp 
CREATE TABLE FogLamp (
	FogLampID INT NOT NULL,
	TestTrayID INT NOT NULL,
	Defective bit NOT NULL,
	CompletionTime DATETIME NOT NULL DEFAULT GETDATE()
	PRIMARY KEY(FogLampID, TestTrayID)
	FOREIGN KEY(TestTrayID) REFERENCES TestTray(TestTrayID)
);
GO

/*******************************
* INSERTIONS                   *
********************************/

-- Configuration settings for default part capacities 
INSERT INTO Config (cName, cValue, cDescription)
VALUES 
 ('Harness', '55', 'This is the default bin capacity for a harness part'),
 ('Reflector', '35', 'This is the default bin capacity for a reflector part'),
 ('Housing', '24', 'This is the default bin capacity for a housing part'),
 ('Lens', '40', 'This is the default bin capacity for a lens part'),
 ('Bulb', '60', 'This is the default bin capacity for a bulb part'),
 ('Bezel', '75', 'This is the default bin capacity for a bezel part'),

-- Configuration settings for time scale 
 ('TimeScale', '50', 'This is the Ratio of time for the simulation (1 minute of our time is n minutes of the simulations)'),

-- Configuration settings for the runner 
 ('PartsRefillThreshold', '5', 'This is the amount of parts left in a bin before the assembler places a KanBan card in the common tray'),
 ('RunnerCycleTime', '300', 'This is the span of time (in seconds) that a runner goes from picking up cards from the common tray, to replenishing bins referenced in the common tray at the time.'),

-- Configuration settings for test trays 
 ('TestTrayCapacity', '60', 'This is the amount of FogLamps capable of being held in a test tray'),

-- Configuration settings for workers assembly time 
 ('DefaultAssemblyTime', '60', 'This is the default amount of time it takes to assemble a FogLamp'),
 ('NormalAssemblyTime', '1.0', 'This is the assembly speed multiplier for an Normal worker'),
 ('SuperAssemblyTime', '0.85', 'This is the assembly speed multiplier for an Experienced worker'),
 ('RookieAssemblyTime', '1.5', 'This is the assembly speed multiplier for a Rookie worker'),

-- Configuration settings for workers defect rates 
 ('NormalDefectRate', '.5', 'This is the percentage of defects that a Experienced/Normal worker will make on average'),
 ('SuperDefectRate', '.15', 'This is the percentage of defects that a Experienced/Super worker will make on average'),
 ('RookieDefectRate', '.85', 'This is the percentage of defects that a New/Rookie worker will make on average');

 -- State
 INSERT INTO SimulationState (isActive) VALUES (1);

-- Parts 
INSERT INTO Parts (PartID,PartName) VALUES
(1,'Harness'),
(2,'Reflector'),
(3,'Housing'),
(4,'Lens'),
(5,'Bulb'),
(6,'Bezel');

-- Employee Types 
INSERT INTO EmployeeType(TypeID,LevelOfExperience) VALUES
(1, 'Normal'),
(2, 'Super'),
(3, 'Rookie');

GO
/*******************************
* FUNCTIONS                    *
********************************/
-- Get the capcity of a test tray from the config table
-- this is a function so we may use it frequently
CREATE FUNCTION dbo.GetTestTrayCapacity ()
RETURNS int
WITH EXECUTE AS CALLER
AS
BEGIN
-- and cast it as an int
 RETURN CAST((SELECT cValue FROM Config WHERE cName = 'TestTrayCapacity') AS int);
END
GO

CREATE FUNCTION dbo.GetPartsThreshold ()
RETURNS int
WITH EXECUTE AS CALLER
AS
BEGIN
-- and cast it as an int
 RETURN CAST((SELECT cValue FROM Config WHERE cName = 'PartsRefillThreshold') AS int);
END
GO

/*******************************
* STORED PROCEDURES            *
********************************/

/* Make FogLam */
-- make a fog lamp, also handling interactions with the test tray it belongs to
CREATE PROCEDURE CreateFogLamp
@defective bit,
@Wid int
AS
-- get the tray id of the current tray we are working on 
DECLARE @trayID int
set @trayID = (SELECT TestTrayID FROM TestTray WHERE WorkshopID = @Wid AND LampCount < (SELECT [dbo].[GetTestTrayCapacity] ()));
/* Make a lamp id from the amount of lamps already there +1 */
DECLARE @lampID int 
set @lampID = (SELECT LampCount FROM TestTray WHERE TestTrayID = @trayID)+1;

/* Now that we have a lampID, increment the lamp count in testtray */
UPDATE TestTray
SET LampCount = @lampID
WHERE TestTrayID = @trayID;

/* Insert a fogLamp entry */
INSERT INTO FogLamp (FogLampID,TestTrayID,Defective) VALUES
(@lampID,@trayID,@defective);
GO

/* Make Workstation */
-- initiate a workstation, this entails giving it bins and a test try as well
CREATE PROCEDURE MakeWorkstation
AS
-- insert a workshop into the table, with a random employee type
INSERT INTO Workshop(EmployeeTypeID, IsActive) values
((SELECT TOP 1 TypeID FROM EmployeeType
ORDER BY NEWID()), 1);

-- we still need the workshop id so store that in a variable
DECLARE @Wid int;
SET @Wid = SCOPE_IDENTITY()

-- use that to make a test try 
INSERT INTO TestTray(WorkshopID,LampCount) VALUES
(@Wid, 0);

-- now we need to loop through the parts, and provide a bin for each
DECLARE @I INT = 1;
WHILE @I < (SELECT COUNT(PartID) FROM Parts)+1
BEGIN
INSERT INTO Bin(PartID,WorkshopID,InUse,PartQuantity) VALUES
(@I,@Wid,1,(SELECT TOP 1 c.cValue FROM Config c WHERE c.cName = (SELECT PartName from Parts WHERE PartName = c.cName AND PartID = @I)));
SET @I = @I +1;
END;
-- then we return the workshop id for the program to use
 RETURN @Wid;
GO

-- this is for providing info for a workstation
CREATE PROCEDURE WorkstationTypeInfo
@workID int,
@DefectRate float OUTPUT,
@AssemblyTime float OUTPUT
AS
-- this uses the level of experience of the employee to figure out the defect rate
SET @DefectRate = (SELECT cValue FROM Config
 WHERE cName = CONCAT(
 (SELECT LevelOfExperience FROM EmployeeType join Workshop on EmployeeTypeID = TypeID WHERE Workshop.WorkshopID = @workID)
 ,'DefectRate'));

 -- and assembly time
 SET @AssemblyTime = (SELECT cValue FROM Config
 WHERE cName = CONCAT(
 (SELECT LevelOfExperience FROM EmployeeType join Workshop on EmployeeTypeID = TypeID WHERE Workshop.WorkshopID = @workID)
 ,'AssemblyTime'));
GO

-- this will return all bins for the workstation the they are currently using
CREATE PROCEDURE GetBins
@workID int
AS
SELECT BinID, PartID, PartQuantity FROM Bin WHERE WorkshopID = @workID AND InUse = 1;
GO

-- this simply provides the timescale set by the configurations
CREATE PROCEDURE GetTimescale
AS
SELECT cValue FROM Config WHERE cName = 'Timescale';
GO

-- this simply provides the DefaultAssemblyTime set by the configurations
create procedure GetDefaultAssemblyTime
AS
select cValue from Config where cName = 'DefaultAssemblyTime';
GO

-- this simply provides the RunnerCycleTime set by the configurations
CREATE PROCEDURE GetRunnerTime
AS
select cValue from Config where cName = 'RunnerCycleTime';
GO

-- this will update the config table taking in a name and value to change it
CREATE PROCEDURE UpdateConfiguration
@cName varchar(50),
@cValue varchar(25)
AS
UPDATE Config
SET cValue = @cValue
WHERE cName = @cName;
GO

-- Reduce Parts
CREATE PROCEDURE ReduceParts
@WorkID int
AS
DECLARE @retCode bit;
BEGIN TRAN
UPDATE Bin
SET PartQuantity = PartQuantity-1
WHERE WorkshopID = @WorkID AND InUse = 1;

-- if theres an error, rollback the transaction
IF @@ERROR <> 0
BEGIN
ROLLBACK TRAN
SET @retCode = 0
GOTO ENDING
END
COMMIT TRAN
SET @retCode = 1
ENDING:
-- then we return the workshop id for the program to use
 RETURN @retCode;
GO

-- ReplenishQueue
create procedure MakeReplenishQueue
@OldBinID int
AS
DECLARE @Wid INT;
DECLARE @PartID int;
DECLARE @Quantity int;


set @Wid = (select WorkshopID FROM Bin WHERE BinID = @OldBinID);
set @PartID = (select PartID FROM Bin WHERE BinID = @OldBinID);
set @Quantity = (select PartQuantity FROM Bin WHERE BinID = @OldBinID);

-- get the part default
set @Quantity = @Quantity+ CAST((SELECT TOP 1 c.cValue FROM Config c WHERE c.cName = (SELECT PartName from Parts WHERE PartName = c.cName AND PartID = @PartID)) as int)
-- update old bins to not be in use
UPDATE Bin
SET InUse = 0, PartQuantity = 0
WHERE BinID = @OldBinID;
-- make new bins for parts
INSERT INTO Bin (PartID, WorkshopID, PartQuantity, InUse ) VALUES
(@PartID,@Wid,@Quantity,1);
GO

-- CommonTrayDump
create procedure DumpCommonTray
@LastRunTime DateTime
AS

SELECT c.BinID FROM CommonTray c JOIN Bin b 
ON c.BinID = b.BinID
WHERE c.EntryTime >= @LastRunTime;
GO

-- Simulation state management
CREATE PROCEDURE OnPause
AS
UPDATE SimulationState
SET isActive = 0;
GO

CREATE PROCEDURE OnResume
AS
UPDATE SimulationState
SET isActive = 1;
GO

CREATE PROCEDURE GetState
AS
DECLARE @state bit;
SET @state = (SELECT TOP 1 isActive FROM SimulationState);
RETURN @state;
GO

-- Visualization Stored Procs
CREATE PROCEDURE GetWorkstationIDs
AS
SELECT WorkshopID FROM Workshop WHERE IsActive = 1;
GO

CREATE PROCEDURE GetAndonBins
@workID int
AS
SELECT b.BinID, b.PartID, b.PartQuantity, c.cValue FROM 
Bin b JOIN Parts p ON p.PartID = b.PartID 
JOIN Config c ON c.cName = p.PartName
WHERE WorkshopID = @workID AND InUse = 1
ORDER BY p.PartID;
GO

-- Threshold for when a kanban card is made
CREATE PROCEDURE GetPartRefillThreshold
AS
DECLARE @threshold int;
SET @threshold = (SELECT [dbo].[GetPartsThreshold] ());
RETURN @threshold
GO

-- update worksation state
CREATE PROCEDURE UpdateWorkstationState
@workID int
AS
UPDATE Workshop
SET IsActive = 0
WHERE WorkshopID = @workID;
GO

-- yield calculations procs
CREATE PROCEDURE GetGoodLampCount
AS
DECLARE @GoodFogCount INT;
SET @GoodFogCount = (SELECT COUNT( FogLampID) FROM FogLamp WHERE Defective = 0);
RETURN @GoodFogCount
GO

CREATE PROCEDURE GetDefectCount
AS
DECLARE @FogCount INT;
SET @FogCount = (SELECT COUNT( FogLampID) FROM FogLamp WHERE Defective = 1);
RETURN @FogCount
GO

CREATE PROCEDURE GetLampCount
AS
DECLARE @FogCount INT;
SET @FogCount = (SELECT COUNT( FogLampID) FROM FogLamp);
RETURN @FogCount
GO

/*******************************
* TRIGGERS			           *
********************************/


-- after insert of foglamp, check if we nee a new tray
CREATE TRIGGER Insert_FogLamp
ON FogLamp AFTER INSERT
AS
IF (SELECT Lampcount FROM TestTray where TestTrayID = (SELECT i.TestTrayID FROM inserted i))  = (SELECT [dbo].[GetTestTrayCapacity] ())
BEGIN
/* Get the workshop id of the old test tray, using the id of the tray we inserted as the foreign key on the fog lamp */
DECLARE @WorkID int;
SET @WorkID = (SELECT WorkshopID FROM TestTray where TestTrayID = (SELECT i.TestTrayID FROM inserted i))

INSERT INTO TestTray (WorkshopID, LampCount) VALUES
(@WorkID,0);
END
GO

-- INSERT A TRIGGER FOR UPDATES ON BIN
CREATE TRIGGER BinUpdateTrigger
ON Bin AFTER UPDATE
AS
BEGIN
INSERT INTO CommonTray (BinID)
SELECT b.BinID 
FROM Bin b
WHERE b.PartQuantity <=(SELECT [dbo].[GetPartsThreshold]()) AND b.InUse = 1
AND NOT EXISTS(SELECT c.BinID FROM CommonTray c
WHERE c.BinID = b.BinID);
END
GO