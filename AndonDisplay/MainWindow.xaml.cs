﻿/*
 * Project      : ASQL Final Project
 * Developer(s) : Nathan Bray, Gabriel Paquette
 * Date Created : 2017-04-20
 * Description  : This class handles the events and proccessing of the kanban simulation results
 */

using KanbanObjects;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Media;

namespace AndonDisplay
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly DataAccess _dal = new DataAccess();
        public List<Bin> BinList = new List<Bin>();
        public List<int> WorkshopId { get; set; }
        public int SelectedWorkshop { get; set; }
        private int defect { get; set; }
        private int goodParts { get; set; }
        private Dictionary<string, int> yield = new Dictionary<string, int>();

        private readonly Timer _timer = new Timer(100);

        // make a chart
        private Chart chart;

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            WorkshopId = _dal.GetWorkstationIDs();
            // setup the chart variables
            chart = FindName("AssemblyChart") as Chart;
            goodParts = _dal.GetGoodLampCount();
            defect = _dal.GetDefectLampCount();
            LstWorkstations.ItemsSource = WorkshopId;

            if (WorkshopId.Count > 0)
            {
                LstWorkstations.SelectedIndex = 0;
                UpdateBars();
            }
            UpdateChart();
            SetConsoleCtrlHandler(ConsoleCtrlCheck, true);
        }

        #region unmanaged

        // Declare the SetConsoleCtrlHandler function
        // as external and receiving a delegate.

        [DllImport("Kernel32")]
        public static extern bool SetConsoleCtrlHandler(HandlerRoutine handler, bool add);

        // A delegate type to be used as the handler routine
        // for SetConsoleCtrlHandler.
        public delegate bool HandlerRoutine(CtrlTypes ctrlType);

        // An enumerated type for the control messages
        // sent to the handler routine.
        public enum CtrlTypes
        {
            CtrlCEvent = 0,
            CtrlBreakEvent,
            CtrlCloseEvent,
            CtrlLogoffEvent = 5,
            CtrlShutdownEvent
        }

        #endregion unmanaged

        /// <summary>
        /// This method will make sure the program processes the closing of the client
        /// </summary>
        /// <param name="ctrlType"></param>
        /// <returns>true</returns>
        private bool ConsoleCtrlCheck(CtrlTypes ctrlType)
        {
            // Put your own handler here
            switch (ctrlType)
            {
                case CtrlTypes.CtrlCEvent:
                case CtrlTypes.CtrlBreakEvent:
                case CtrlTypes.CtrlCloseEvent:
                case CtrlTypes.CtrlShutdownEvent:
                    CloseApplication();
                    break;
            }
            return true;
        }

        /// <summary>
        /// this will stop the timer when we close
        /// </summary>
        private void CloseApplication()
        {
            _timer.Stop();
            _timer.Dispose();
        }

        /// <summary>
        /// This method will update the values of the progress bars
        /// and will update the chart
        /// </summary>
        private void UpdateBars()
        {
            _timer.Elapsed += (sender, e) =>
            {
                // if we have selected something from the list
                if (SelectedWorkshop >= 0)
                {
                    // get the workshop and bin list
                    WorkshopId.Clear();
                    WorkshopId = _dal.GetWorkstationIDs();
                    BinList = _dal.GetBins(SelectedWorkshop, true);

                    try
                    {
                        Dispatcher.Invoke(() =>
                        {
                            // update the chart
                            UpdateChart();
                            LstWorkstations.ItemsSource = WorkshopId;
                            // if we have all the bins
                            if (BinList.Count == 6)
                            {
                                // populate the values and maximums
                                PbHarness.Maximum = BinList[0].MaxValue;
                                PbReflector.Maximum = BinList[1].MaxValue;
                                PbHousing.Maximum = BinList[2].MaxValue;
                                PbLens.Maximum = BinList[3].MaxValue;
                                PbBulb.Maximum = BinList[4].MaxValue;
                                PbBezel.Maximum = BinList[5].MaxValue;

                                PbHarness.Value = BinList[0].Quantity;
                                PbReflector.Value = BinList[1].Quantity;
                                PbHousing.Value = BinList[2].Quantity;
                                PbLens.Value = BinList[3].Quantity;
                                PbBulb.Value = BinList[4].Quantity;
                                PbBezel.Value = BinList[5].Quantity;
                            }
                            //update the progress bar
                            UpdateProgressColour(PbBezel, PbBulb, PbHarness, PbLens, PbReflector, PbHousing);
                        });
                    }
                    catch (Exception)
                    {
                        _timer.Stop();
                    }
                }
            };
        }

        /// <summary>
        /// This method will set our index to the selected list item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LstWorkstations_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListView item = (ListView)sender;
            int workshop = -1;
            try
            {
                if (int.TryParse(item.SelectedItem.ToString(), out workshop))
                {
                    _timer.Stop();
                    SelectedWorkshop = workshop;
                    _timer.Start();
                }
            }
            catch (Exception)
            {
                _timer.Stop();
                SelectedWorkshop = workshop;
            }
        }

        /// <summary>
        /// This method will change the colour of the progress bars to red when
        /// the value reaches 5
        /// </summary>
        /// <param name="bars"></param>
        private void UpdateProgressColour(params ProgressBar[] bars)
        {
            foreach (ProgressBar progBar in bars)
            {
                if (progBar.Value <= 5)
                {
                    progBar.Foreground = Brushes.Red;
                }
                else
                {
                    progBar.Foreground = Brushes.LimeGreen;
                }
            }
        }

        /// <summary>
        /// This method updates the chart values
        /// </summary>
        private void UpdateChart()
        {
            if (chart != null)
            {
                // clear all points and reset the values in the dictionary
                chart.Series["series"].Points.Clear();
                goodParts = _dal.GetGoodLampCount();
                defect = _dal.GetDefectLampCount();
                yield.Clear();
                yield.Add("Product", goodParts);
                yield.Add("Defects", defect);
                chart.DataSource = yield;
                // refresh the series values
                chart.Series["series"].LegendText = "#VALX";
                chart.Series["series"].XValueMember = "Key";
                chart.Series["series"].YValueMembers = "Value";
                chart.DataBind();
                chart.Update();
            }
        }
    }
}