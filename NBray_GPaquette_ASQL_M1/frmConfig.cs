﻿/*
 * Project      : ASQL Final Project
 * Developer(s) : Nathan Bray, Gabriel Paquette
 * Date Created : 2017-04-01
 * Description  : This file handles the events by the controls of the from
 * This consists of enabling and disabling certain controls to allow the
 * 'pausing' and 'starting' of the KanBan system
 */

using KanbanObjects;
using System;
using System.Windows.Forms;

namespace NBray_GPaquette_ASQL_M1
{
    public partial class FrmConfig : Form
    {
        // our DAL
        private readonly DataAccess _dal;

        public FrmConfig()
        {
            InitializeComponent();
            _dal = new DataAccess();
            // disable everything initially so the user has to hit 'update' to actually change anything
            btnSave.Enabled = false;
            btnCancel.Enabled = false;
            txtValue.Enabled = false;
            txtDescription.Enabled = false;
            dgvConfig.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }

        private void frmConfig_Load(object sender, EventArgs e)
        {
            // fill the dataAdapter/ our dataGridView with the information gathered from our data source
            this.configTableAdapter.Fill(this.KanbanDataset.Config);
        }

        /// <summary>
        /// This method will populate our text boxes with the innformation in the cell the user clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvConfig_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvConfig.SelectedCells.Count > 0)
            {
                int selectedrowindex = dgvConfig.SelectedCells[0].RowIndex;

                DataGridViewRow selectedRow = dgvConfig.Rows[selectedrowindex];

                // take the selected row and populate the text fields with the information
                txtName.Text = Convert.ToString(selectedRow.Cells[0].Value);
                txtValue.Text = Convert.ToString(selectedRow.Cells[1].Value);

                txtDescription.Text = (string)selectedRow.Cells[2].Value;
            }
        }

        /// <summary>
        /// this method will take the value put in the value text field, validate it,
        /// and update the value in the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            double value;
            if (!string.IsNullOrEmpty(txtValue.Text) && double.TryParse(txtValue.Text, out value))
            {
                if (value > 0)
                {
                    if (dgvConfig.SelectedCells.Count > 0)
                    {
                        int selectedrowindex = dgvConfig.SelectedCells[0].RowIndex;
                        DataGridViewRow selectedRow = dgvConfig.Rows[selectedrowindex];

                        selectedRow.Cells[1].Value = txtValue.Text;

                        _dal.UpdateValue(value, (string)selectedRow.Cells[0].Value);
                        btnCancel_Click(sender, e);
                    }
                }
            }
        }

        /// <summary>
        /// this method will enable controls so that when the user wishes to update,
        /// we can 'pause' the KanBan system
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            btnSave.Enabled = true;
            btnCancel.Enabled = true;
            txtValue.Enabled = true;
            _dal.PauseSimulation();
        }

        /// <summary>
        /// this method will disable controls so that when the user cancels, the KanBan system will 'resume'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            btnSave.Enabled = false;
            btnCancel.Enabled = false;
            txtValue.Enabled = false;
            _dal.ResumeSimulation();
        }
    }
}