﻿/*
 * Project      : ASQL Final Project
 * Developer(s) : Nathan Bray, Gabriel Paquette
 * Date Created : 2017-04-20
 * Description  : This class handles the starting of the Runner program in our simulation
 */

using System;
using System.Configuration;

namespace RunnerProgram
{
    internal class Program
    {
        /// <summary>
        /// This is main, a Runner is created, and the assembly process for it is started
        /// </summary>
        /// <param name="args"></param>
        private static void Main(string[] args)
        {
            string pcName = ".";
            if (args.Length > 0)
            {
                pcName = args[0];
            }

            //If there is an arguement passed in, we use that to see where we have to connect to find the DB
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var connectionStringsSection = (ConnectionStringsSection)config.GetSection("connectionStrings");
            connectionStringsSection.ConnectionStrings["kanbanConnectionString"].ConnectionString = "Data Source=" + pcName + @"\sqldeveloper;Initial Catalog=Kanban;" +
                "Persist Security Info=True;User ID=UserKanban;Password=n0rBertPa$sw0Rds;";
            config.Save();

            ConfigurationManager.RefreshSection("connectionStrings");
            Runner runner = new Runner();
            runner.StartRunner();
            // To prevent the window from closing
            Console.ReadKey();
        }
    }
}