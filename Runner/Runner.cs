﻿/*
 * Project      : ASQL Final Project
 * Developer(s) : Nathan Bray, Gabriel Paquette
 * Date Created : 2017-04-20
 * Description  : This class handles keeping track of the bins the work station uses
 *           and refils them when needed.
 */

using KanbanObjects;
using System;
using System.Collections.Generic;
using System.Timers;

namespace RunnerProgram
{
    public class Runner
    {
        private readonly DataAccess _dal;

        public List<Bin> Bins;
        public int RunnerInterval { get; set; }
        private DateTime LastRun { get; set; }

        /// <summary>
        /// Name: Runner
        /// Description: This is the constructor for the runner.
        /// </summary>
        public Runner()
        {
            _dal = new DataAccess();
            RunnerInterval = _dal.GetRunnerInterval();
            Bins = new List<Bin>();
        }

        /// <summary>
        /// Name: startRunner
        /// Description: This function starts the runner timer, which checks the common tray and
        /// replenishes the bins as needed
        /// </summary>
        public void StartRunner()
        {
            //give it an initial build time
            Timer timer = new Timer(RunnerInterval * 1000 / _dal.GetTimeScale());
            LastRun = DateTime.Now;
            timer.Elapsed += (sender, e) =>
            {
                // get all of the kanban cards from the tray
                Bins = _dal.GetCommonTray(LastRun);

                if (_dal.GetSimulationState() == 1)
                {
                    int interval = (_dal.GetRunnerInterval() * 1000 / _dal.GetTimeScale());
                    if (interval != timer.Interval)
                    {
                        timer.Interval = interval;
                    }
                    //if the runner has kanban cards
                    if (Bins.Count > 0)
                    {
                        //replenish the bins of the cards that the runner has
                        foreach (Bin b in Bins)
                        {
                            _dal.ReplenishQueue(b.BinId);
                        }
                        //get rid off all the kanban cards
                        Bins.Clear();
                        Console.WriteLine("Runner refilled bins and checked common tray");
                    }
                    else
                    {
                        Console.WriteLine("Runner checked on the Common Tray");
                    }

                    LastRun = DateTime.Now;
                }
            };
            timer.Start();
        }
    }
}