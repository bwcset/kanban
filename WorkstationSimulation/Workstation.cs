﻿/*
 * Project      : ASQL Final Project
 * Developer(s) : Nathan Bray, Gabriel Paquette
 * Date Created : 2017-04-20
 * Description  : This class handles all the data and functionality of a workstation in
 *           the kanban simulation.
 */

using KanbanObjects;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Timers;

namespace WorkstationSimulation
{
    public class Workstation
    {
        public List<Bin> Bins = new List<Bin>();
        private readonly DataAccess _dal;
        public int WorkstationId { get; set; }
        public double DefectRate { get; set; }
        public double AssemblyTime { get; set; }
        public int DefaultAssemblyTime { get; set; }
        public int TimeScale { get; set; }
        public int FinalBuildTime { get; set; }

        /// <summary>
        /// Name: Workstation
        /// Description: This is the constuctor for the workstation class.
        /// </summary>
        public Workstation()
        {
            //create data accsss layer
            _dal = new DataAccess();
            //creates a work station in the database and all of this needed conponents
            WorkstationId = _dal.CreateWorkstation();
            Init();
            SetConsoleCtrlHandler(ConsoleCtrlCheck, true);
        }

        #region unmanaged

        // Declare the SetConsoleCtrlHandler function
        // as external and receiving a delegate.

        [DllImport("Kernel32")]
        public static extern bool SetConsoleCtrlHandler(HandlerRoutine handler, bool add);

        // A delegate type to be used as the handler routine
        // for SetConsoleCtrlHandler.
        public delegate bool HandlerRoutine(CtrlTypes ctrlType);

        // An enumerated type for the control messages
        // sent to the handler routine.
        public enum CtrlTypes
        {
            CtrlCEvent = 0,
            CtrlBreakEvent,
            CtrlCloseEvent,
            CtrlLogoffEvent = 5,
            CtrlShutdownEvent
        }

        #endregion unmanaged

        /// <summary>
        /// This method will make sure the program processes the closing of the client
        /// </summary>
        /// <param name="ctrlType"></param>
        /// <returns>true</returns>
        private bool ConsoleCtrlCheck(CtrlTypes ctrlType)
        {
            // Put your own handler here
            switch (ctrlType)
            {
                case CtrlTypes.CtrlCEvent:
                case CtrlTypes.CtrlBreakEvent:
                case CtrlTypes.CtrlCloseEvent:
                case CtrlTypes.CtrlShutdownEvent:
                    CloseApplication();
                    break;
            }
            return true;
        }

        /*
        Name: CloseApplication
        Description: This function is called when the work station is closed.
                     It tells the DB the current work station is not in use anymore.
        */

        private void CloseApplication()
        {
            _dal.TurnOffWorkstation(WorkstationId);
        }

        /// <summary>
        /// Name: Init
        /// Description: This function initializes all settings for the work station
        ///              with all the configurations in the DB.
        /// </summary>
        public void Init()
        {
            double defect;
            double assembly;

            //gets workstation information
            _dal.GetWorkstationInfo(WorkstationId, out defect, out assembly);
            DefectRate = defect;
            AssemblyTime = assembly;

            DefaultAssemblyTime = _dal.GetDefaultAssemblyTime();

            Bins = _dal.GetBins(WorkstationId);
            TimeScale = _dal.GetTimeScale();

            FinalBuildTime = Convert.ToInt32(CalulateTotalBuildTime());
        }

        /// <summary>
        /// Name: calulateTotalBuildTime
        /// Description: This is this function caluculates how long it takes to make a lamp        /// </summary>
        /// <returns>the totalBuildTime</returns>
        public double CalulateTotalBuildTime()
        {
            //calculate the total build time of a foglamp in milliseconds
            return ((DefaultAssemblyTime * AssemblyTime) / TimeScale) * 1000;
        }

        /// <summary>
        /// Name: plusOrMinus
        /// Description: This is this function returns a percentage value between 90 and 110
        /// </summary>
        /// <returns>a value between 90 and 110</returns>
        public int PlusOrMinus()
        {
            Random r = new Random(DateTime.Now.Millisecond);
            //returns a range between 90 and 110
            return r.Next() % 20 + 90;
        }

        /// <summary>
        /// Name: tryCreateLamp
        /// Description: This  function checks to see if a lamp can be made, if it ca workn,
        /// it tries to make a lamp
        /// </summary>
        /// <returns>true if a foglamp was made and false otherwise</returns>
        public bool TryCreateLamp()
        {
            bool enoughParts = true;
            foreach (Bin b in Bins)
            {
                //check if a lamp can be made locally
                if (b.Quantity <= 0)
                {
                    enoughParts = false;
                    break;
                }
            }

            //if a lamp can be made, make one
            if (enoughParts)
            {
                enoughParts = BuildLamp();
            }

            return enoughParts;
        }

        /// <summary>
        /// Name: startAssembly
        /// Description: This is this function starts the timer that triggers to create a foglamp
        /// </summary>
        public void StartAssembly()
        {
            //give it an initial build time
            Timer timer = new Timer(FinalBuildTime * (PlusOrMinus() / 100.0));

            bool worked = false;
            timer.Elapsed += (sender, e) =>
            {
                if (_dal.GetSimulationState() == 1)
                {
                    Init();

                    //try to create a lamp
                    worked = TryCreateLamp();

                    //everytime a lamp is attempted to be build, the total build time changes
                    //bewteen a range of +%10 to -%10
                    timer.Interval = FinalBuildTime * (PlusOrMinus() / 100.0);

                    if (worked)
                    {
                        Console.WriteLine("Lamp was built");
                    }
                    else
                    {
                        Console.WriteLine("Not enough parts");
                    }
                }
            };
            timer.Start();
        }

        /// <summary>
        /// Name: buildLamp
        /// Description: This function tries to create a lamp and reduce the parts used to make it
        /// </summary>
        /// <returns>Whether the creation of a fogLamp worked</returns>
        private bool BuildLamp()
        {
            //reduce parts, if the reduce was successful, create a new foglamp and reduce local values
            bool worked = _dal.ReduceParts(WorkstationId);
            if (worked)
            {
                foreach (Bin b in Bins)
                {
                    b.Quantity--;

                    if (b.Quantity == 5)
                    {
                        Console.WriteLine("Creating kanban card for bin: " + b.BinId + " containing part: " + b.PartId);
                    }
                }

                //create foglamp
                FogLamp fl = new FogLamp(DefectRate);
                _dal.AddFoglamp(fl.IsDefective, WorkstationId);
            }
            return worked;
        }
    }
}