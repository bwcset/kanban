﻿/*
 * Project      : ASQL Final Project
 * Developer(s) : Nathan Bray, Gabriel Paquette
 * Date Created : 2017-04-20
 * Description  : This class starts the workstaion simulation
 */

using System;
using System.Configuration;

namespace WorkstationSimulation
{
    internal class Program
    {
        /// <summary>
        /// This is main, a workstation is created, and the assembly process for it is started
        /// </summary>
        /// <param name="args"></param>
        private static void Main(string[] args)
        {
            string pcName = ".";
            if (args.Length > 0)
            {
                pcName = args[0];
            }

            //If there is an arguement passed in, we use that to see where we have to connect to find the DB
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var connectionStringsSection = (ConnectionStringsSection)config.GetSection("connectionStrings");
            connectionStringsSection.ConnectionStrings["kanbanConnectionString"].ConnectionString = "Data Source=" + pcName + @"\sqldeveloper;Initial Catalog=Kanban;" +
                                                                                                    "Persist Security Info=True;User ID=UserKanban;Password=n0rBertPa$sw0Rds;";
            config.Save();
            ConfigurationManager.RefreshSection("connectionStrings");
            Workstation workStation = new Workstation();

            workStation.StartAssembly();
            //read key so the program doesn't close
            Console.ReadKey();
        }
    }
}